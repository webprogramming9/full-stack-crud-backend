import { IsNotEmpty } from 'class-validator';
import { Length, Min } from 'class-validator/types/decorator/decorators';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(3, 64)
  name: string;

  @IsNotEmpty()
  @Min(5)
  price: number;
}
